$(function() {

	$('.expand-button').on('click', function(){
		var panelId = $(this).attr('data-panelid');
		$('#'+panelId).slideToggle(200);
	});

	$('.go-down-button').click(function (e) {
	    var self = $(this),
	        item = self.parents('div.item'),
	        swapWith = item.next();
	    item.before(swapWith.detach());
	});
	$('.go-up-button').click(function (e) {
	    var self = $(this),
	        item = self.parents('div.item'),
	        swapWith = item.prev();
	    item.after(swapWith.detach());
	});
});
