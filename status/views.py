from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import StatusForm
from .models import Status


def index(request):
    context={}

    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            # Parse the required data from form
            # Note that you need to use "cleaned_data" in order to get cleaned data (duh)
            # To tell the differences between clean and raw form data,
            # you can do print(form) and print(form.cleaned_data)
            name = form.cleaned_data['name']
            status = form.cleaned_data['status']

            # Save the newly-posted status on our database/models
            # You can save it by creating new models object
            Status.objects.create(name=name, status=status)
            print(Status.objects.get(status=status))

            return HttpResponseRedirect('/status')

    else:
        form = StatusForm()

    context["form"] = form
    context["statuses"] = Status.objects.all()

    return render(request, 'status/index.html', context)
