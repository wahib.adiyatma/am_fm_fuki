from django import forms

class StatusForm(forms.Form):
    name = forms.CharField(max_length=55, widget=forms.TextInput())
    status = forms.CharField(widget=forms.Textarea(attrs={'placeholder':'Apa yang anda pikirkan ?'}))
